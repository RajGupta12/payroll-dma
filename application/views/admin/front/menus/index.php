<?= message_box('success'); ?>
<?= message_box('error');
$created = can_action('151', 'created');
$deleted = can_action('151', 'deleted');
?>

<div class="panel panel-custom" style="border: none;" data-collapsed="0">
    <div class="panel-heading">
        <div class="panel-title">
            <?= lang('menu_list') ?>
            <div class="pull-right hidden-print" style="padding-top: 0px;padding-bottom: 8px">
                <a href="<?= base_url() ?>admin/front/menus/add_menu" class="btn btn-xs btn-info"
                   data-toggle="modal"
                   data-placement="top" data-target="#myModal">
                    <i class="fa fa-plus "></i> <?= ' ' . lang('add') . ' ' . lang('menu') ?></a>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th><?= lang('title') ?></th>
                <th width="80px"><?= lang('action') ?></th>
            </tr>
            </thead>

            <tbody>
            <script type="text/javascript">
                $(document).ready(function () {
                    list = base_url + "admin/front/menus/menu_list";
                });
            </script>
            </tbody>
        </table>
    </div>
</div>