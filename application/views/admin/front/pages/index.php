<?= message_box('success'); ?>
<?= message_box('error');
$created = can_action('55', 'created');
$edited = can_action('55', 'edited');
$deleted = can_action('55', 'deleted');
?>
<!-- Custom CSS -->




<style type="text/css">
    html {
        font-size: 62.5%;
        -webkit-font-smoothing: antialiased;
    }

    body {
        font-family: "Source Sans Pro", sans-serif;
        font-size: 1.3rem;
        color: #262626;
        line-height: 1.857143;
    }

    .content-wrapper .btn-success {
        color: #fff;
        background-color: #27c24c;
    }

    .content-wrapper .btn-success:hover {
        color: #fff;
        background-color: #24b145;
    }

    .content-wrapper .btn-group-xs > .btn, .btn-xs {
        padding: 0px 5px !important;
        font-size: 10px;
        line-height: 1.5;
    }

    .content-wrapper .radio-inline input[type="radio"] {
        margin-top: 8px;
    }

    .content-wrapper .btn,
    .content-wrapper .btn:focus,
    .content-wrapper .btn:active,
    .chat_frame .btn {
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        touch-action: manipulation;
        cursor: pointer;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 4px 14px;
        font-size: 14px;
        line-height: 1.52857143;
        border-radius: 0;
        background-image: none;
    }

    .table-responsive .form-control,
    .form-group input[type="text"] {
        border-radius: 0 !important;
        border-color: #dddddd !important;
        padding: 4px 14px !important;
    }

    .content-heading .text-muted {
        color: #909fa7;
    }

    .panel-body .btn-primary {
        color: #fff;
        background-color: #5d9cec;
    }

    .panel-body .btn-primary:hover {
        color: #fff;
        background-color: #4b91ea;
    }

    .note-toolbar .btn {
        font-size: 13px;
        border-color: transparent;
        -webkit-appearance: none;
        outline: none !important;
        -webkit-transition: all 0.1s;
        -o-transition: all 0.1s;
        transition: all 0.1s;
        color: #333;
        padding: 0px 12px;
    }

    .note-toolbar .btn.btn-default {
        border-color: #dde6e9;
        background: #f4f4f4;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>plugins/summernote/summernote.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>plugins/summernote/summernote.min.js">

<div class="row">
    <div class="col-sm-12">
        <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/kanban/kan-app.css"/>
        <div class="app-wrapper">
            <p class="total-card-counter" id="totalCards"></p>
            <div class="board" id="board"></div>
        </div>

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : ''; ?>">
                    <a href="#pages" data-toggle="tab"><?= lang('all') ?> <?= lang('pages') ?></a>
                </li>
                <li class="<?= $active == 2 ? 'active' : ''; ?>">
                    <a href="#create" data-toggle="tab"><?= lang('new') ?> <?= lang('page') ?></a>
                </li>
            </ul>
        </div>

        <!--Tab content-->
        <div class="tab-content bg-white">
            <!--All Pages-->
            <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="pages">
                <div class="table-responsive">

                    <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><?= lang('title') ?></th>
                            <th><?= lang('url') ?></th>
                            <th><?= lang('page') ?> <?= lang('type') ?></th>
                            <th class="col-options no-sort"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                list = base_url + "admin/front/page/pageList";

                            });
                        </script>
                        </tbody>
                    </table>
                </div>
            </div>

            <!--Create or Edit Page-->
            <?php if (!empty($created) || !empty($edited)) {
                if (!empty($page_info)) {
                    $pages_id = $page_info->pages_id;
                } else {
                    $pages_id = null;
                }
                ?>
                <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
                    <form role="form" enctype="multipart/form-data"
                          data-parsley-validate="" novalidate=""
                          action="<?php echo base_url(); ?>admin/front/page/create/<?= $pages_id ?>" method="post"
                          class="form-horizontal">

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-md-2 text-right"><?= lang('title') ?> <span
                                            class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" value="<?php
                                    if (!empty($page_info)) {
                                        echo $page_info->title;
                                    }
                                    ?>" name="title" required="">
                                    <?php
                                    if (!empty($page_info)) {
                                        ?>
                                        <input type="hidden" class="form-control" value="<?php
                                        if (!empty($page_info)) {
                                            echo $page_info->slug;
                                        }
                                        ?>" name="slug" required="">
                                    <?php } ?>
                                </div>
                            </div>

                            <input type="hidden" name="content_category" value="standard">

                            <div class="form-group">
                                <label class="control-label"> <?= lang('description') ?> <span
                                            class="text-danger">*</span></label>

                                <div class="pull-right hidden-print" style="padding-top: 0px;padding-bottom: 8px">
                                    <a href="<?= base_url() ?>admin/front/page/add_image" class="btn btn-xs btn-info"
                                       data-toggle="modal"
                                       data-placement="top" data-target="#myModal_large">
                                        <i class="fa fa-plus "></i> <?= ' ' . lang('add') . ' ' . lang('media') ?></a>
                                </div>

                                <textarea name="description"
                                          class="summernote"><?= !empty($page_info->description) ? $page_info->description : ' ' ?></textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary pull-right"><i
                                            class="fa fa-save mr-sm"></i> Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 350,
            tabsize: 2
        });
    });
</script>