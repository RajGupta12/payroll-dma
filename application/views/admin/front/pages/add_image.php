<style>
    .modal-footer .btn {
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        touch-action: manipulation;
        cursor: pointer;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 4px 14px;
        font-size: 14px;
        line-height: 1.52857143;
        border-radius: 0;
        background-image: none;
    }
    .modal-footer .btn-default {
        color: #1a2127;
        border: 1px solid #dddd;
    }
    .modal-footer .btn-primary {
        color: #fff;
        background-color: #5d9cec;
    }

    .modal-footer .btn-primary:hover {
        color: #fff;
        background-color: #4b91ea;
    }
</style>
<div class="panel panel-custom" style="border: none;" data-collapsed="0">
    <div class="panel-heading">
        <div class="panel-title">Add Media</div>
    </div>

    <div class="modal-body">
        <div class="mediarow">
            <div class="row" id="media_div"></div>
        </div>
        <div align="right" id="pagination_link"></div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary add_media">Add</button>
    </div>
</div>

<script type="text/javascript">
    $('#myModal_large').on('loaded.bs.modal', function () {
        load(1);
    });
    $(document).on("click", ".pagination li a", function (event) {
        event.preventDefault();
        var page = $(this).data("ci-pagination-page");
        load(page);
    });
    $(".search_text").keyup(function () {
        load(1);
    });
    $(".file_type").change(function () {
        load(1);
    });

    $(document).on('click', '.img_div_modal', function (event) {
        $('.img_div_modal div.fadeoverlay').removeClass('active');
        $(this).closest('.img_div_modal').find('.fadeoverlay').addClass('active');
    });
    $(document).on('click', '.add_media', function (event) {
        var content_html = $('div#media_div').find('.fadeoverlay.active').find('img').data('img');
        var is_image = $('div#media_div').find('.fadeoverlay.active').find('img').data('is_image');
        var content_name = $('div#media_div').find('.fadeoverlay.active').find('img').data('content_name');
        var content_type = $('div#media_div').find('.fadeoverlay.active').find('img').data('content_type');
        var vid_url = $('div#media_div').find('.fadeoverlay.active').find('img').data('vid_url');
        var content = "";

        if (typeof content_html !== "undefined") {
            if (is_image === 1) {
                content = '<img src="' + content_html + '">';
            } else if (content_type == "video") {

                var youtubeID = YouTubeGetID(vid_url);

                content = '<iframe id="video" width="420" height="315" src="//www.youtube.com/embed/' + youtubeID + '?rel=0" frameborder="0" allowfullscreen></iframe>';

            } else {
                content = '<a href="' + content_html + '">' + content_name + '</a>';

            }
            InsertHTML(content);
            $('#myModal_large').modal('hide');
        }


    });

    function InsertHTML(content) {
        $(".summernote").summernote('pasteHTML',content);

        // var editor = $.summernote.eventHandler.getEditor();
        // var editable=$('.note-editable');
        // // var html = $('<img src="'+url+'" />');
        // editor.insertNode(editable, content);
        // $('.summernote').summernote('insertNode', content);
        // console.log(content_html);
        // var summernote = $(".summernote");
        // summernote.insertNode(content, true);
        // summernote.summernote("insertNode", content);
    }


    function YouTubeGetID(url) {
        var ID = '';
        url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            ID = ID[0];
        } else {
            ID = url;
        }
        return ID;
    }

    function load(page) {
        var keyword = $('.search_text').val();
        var file_type = $('.file_type').val();
        var is_gallery = 0;
        $.ajax({
            url: "<?php echo base_url(); ?>admin/front/media/getPage/" + page,
            method: "GET",
            data: {'keyword': keyword, 'file_type': file_type, 'is_gallery': is_gallery},
            dataType: "json",
            beforeSend: function () {
                $('#media_div').empty();
            },
            success: function (data) {
                $('#media_div').empty();
                if (data.result_status === 1) {
                    $.each(data.result, function (index, value) {
                        $("#media_div").append(data.result[index]);
                    });
                    $('#pagination_link').html(data.pagination_link);
                } else {
                }
            },
            complete: function () {

            }
        });
    }

    $(document).on('click', '.btn_delete', function () {
        var $this = $('.btn_delete');

        var record_id = $('#record_id').val();
        $.ajax({
            url: "<?php echo base_url(); ?>admin/front/media/deleteItem",
            type: "POST",
            data: {'record_id': record_id},
            dataType: 'Json',
            beforeSend: function () {
                $this.button('loading');
            },
            success: function (data, textStatus, jqXHR) {
                if (data.status === 'success') {
                    load(1);
                }
                $("#confirm-delete").modal('hide');
                toastr[data.status](data.msg);
            },

            complete: function () {

                $this.button('reset');
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    });

    function convertSize(bytes, decimalPoint) {
        if (bytes == 0)
            return '0 Bytes';
        var k = 1024,
            dm = decimalPoint || 2,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
</script>