<?php echo message_box('success') ?>
<?php echo message_box('error') ?>
<div class="row">
    <!-- Start Form -->
    <div class="col-lg-12">
        <form role="form" id="form" action="<?php echo base_url(); ?>admin/front/settings/save_contact" method="post" enctype="multipart/form-data" class="form-horizontal  ">
            <section class="panel panel-custom">
                <header class="panel-heading"><?= lang('contact_info') ?></header>
                <div class="panel-body pb0">

                    <div class="form-group">
                        <label class="col-lg-3 control-label"> Title </label>
                        <div class="col-lg-6">
                            <input type="text" name="front_contact_title" class="form-control" value="<?php if (config_item('front_contact_title') != '') { echo config_item('front_contact_title'); }?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Description </label>
                        <div class="col-lg-6">
                            <textarea name="front_contact_description" class="form-control" rows="4"><?php if (config_item('front_contact_description') != '') { echo config_item('front_contact_description'); }?></textarea>
                        </div>
                    </div>

<!--                    <div class="form-group">-->
<!--                        <label class="col-lg-3 control-label"> Location </label>-->
<!--                        <div class="col-lg-6">-->
<!--                            <input type="text" name="front_contact_location" class="form-control" value="--><?php //if (config_item('front_contact_location') != '') { echo config_item('front_contact_location'); }?><!--">-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <div class="form-group">-->
<!--                        <label class="col-lg-3 control-label"> Support </label>-->
<!--                        <div class="col-lg-6">-->
<!--                            <input type="text" name="front_contact_number" class="form-control" value="--><?php //if (config_item('front_footer_number') != '') { echo config_item('front_contact_number'); }?><!--">-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <div class="form-group">-->
<!--                        <label class="col-lg-3 control-label"> Email </label>-->
<!--                        <div class="col-lg-6">-->
<!--                            <input type="text" name="front_contact_email" class="form-control" value="--><?php //if (config_item('front_contact_email') != '') { echo config_item('front_contact_email'); }?><!--">-->
<!--                        </div>-->
<!--                    </div>-->

                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
        <!-- End Form -->
    </div>
</div>
