<?php echo message_box('success') ?>
<?php echo message_box('error') ?>
<style>
    .material-switch > input[type="checkbox"] {
        display: none;
    }

    .material-switch > label {
        cursor: pointer;
        height: 0px;
        position: relative;
        width: 40px;
    }

    .material-switch > label::before {
        background: rgb(0, 0, 0);
        box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
        border-radius: 8px;
        content: '';
        height: 16px;
        margin-top: -8px;
        position: absolute;
        opacity: 0.3;
        transition: all 0.4s ease-in-out;
        width: 40px;
    }

    .material-switch > label::after {
        background: rgb(255, 255, 255);
        border-radius: 16px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
        content: '';
        height: 24px;
        left: -4px;
        margin-top: -8px;
        position: absolute;
        top: -4px;
        transition: all 0.3s ease-in-out;
        width: 24px;
    }

    .material-switch > input[type="checkbox"]:checked + label::before {
        background: inherit;
        opacity: 0.5;
    }

    .material-switch > input[type="checkbox"]:checked + label::after {
        background: inherit;
        left: 20px;
    }
</style>

<div class="row">
    <!-- Start Form -->
    <div class="col-lg-12">
        <form role="form" id="form" action="<?php echo base_url(); ?>admin/front/settings/general_settings"
              method="post" enctype="multipart/form-data" class="form-horizontal  ">
            <section class="panel panel-custom">
                <header class="panel-heading"><?= lang('general_settings') ?>
                    <?php if (config_item('sync_frontend') != 'Done') { ?>
                        <div class="pull-right">
                            <a href="<?= base_url('admin/front/settings/sync_frontend') ?>"><?= lang('sync_frontend') ?></a>
                        </div>
                    <?php } ?>
                </header>
                <div class="panel-body pb-sm">
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo lang('site_name') ?> </label>

                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_site_name') != '') {
                                echo config_item('front_site_name');
                            } ?>" name="front_site_name" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo lang('favicon') ?> (32px X 32px)</label>
                        <div class="col-lg-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 210px;">
                                    <?php if (config_item('front_favicon') != '') : ?>
                                        <img src="<?php echo base_url() . config_item('front_favicon'); ?>">
                                    <?php else: ?>
                                        <img src="http://placehold.it/32x32"
                                             alt="Please Connect Your Internet">
                                    <?php endif; ?>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="width: 210px;"></div>
                                <div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileinput-new">
                                            <input type="file" name="front_favicon" value="upload" class="form-controll"
                                                   data-buttonText="<?= lang('choose_file') ?>" id="myImg"/>
                                            <span class="fileinput-exists"><?= lang('change') ?></span>
                                        </span>
                                            <a href="#" class="btn btn-default fileinput-exists"
                                               data-dismiss="fileinput"><?= lang('remove') ?></a>
                                    </span>
                                </div>

                                <div id="valid_msg" style="color: #e11221"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('site_logo'); ?> (150px X 140px)</label>
                        <div class="col-lg-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 210px;">
                                    <?php if (config_item('front_nav_logo') != '') : ?>
                                        <img src="<?php echo base_url() . config_item('front_nav_logo'); ?>">
                                    <?php else: ?>
                                        <img src="http://placehold.it/32x32"
                                             alt="Please Connect Your Internet">
                                    <?php endif; ?>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="width: 210px;"></div>
                                <div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileinput-new">
                                            <input type="file" name="front_nav_logo" value="upload"
                                                   class="form-controll" data-buttonText="<?= lang('choose_file') ?>"
                                                   id="myImg"/>
                                            <span class="fileinput-exists"><?= lang('change') ?></span>
                                        </span>
                                            <a href="#" class="btn btn-default fileinput-exists"
                                               data-dismiss="fileinput"><?= lang('remove') ?></a>
                                    </span>
                                </div>

                                <div id="valid_msg" style="color: #e11221"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('header_image'); ?></label>
                        <div class="col-lg-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 210px;">
                                    <?php if (config_item('front_header_image') != '') : ?>
                                        <img src="<?php echo base_url() . config_item('front_header_image'); ?>">
                                    <?php else: ?>
                                        <img src="http://placehold.it/32x32"
                                             alt="Please Connect Your Internet">
                                    <?php endif; ?>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="width: 210px;"></div>
                                <div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileinput-new">
                                            <input type="file" name="front_header_image" value="upload"
                                                   class="form-controll" data-buttonText="<?= lang('choose_file') ?>"
                                                   id="myImg"/>
                                            <span class="fileinput-exists"><?= lang('change') ?></span>
                                        </span>
                                            <a href="#" class="btn btn-default fileinput-exists"
                                               data-dismiss="fileinput"><?= lang('remove') ?></a>
                                    </span>
                                </div>

                                <div id="valid_msg" style="color: #e11221"></div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('home_page_slider') ?></label>

                        <div class="col-lg-6">
                            <div class="material-switch" style="margin-top: 7px;">
                                <input name="front_slider" id="ext_url" type="checkbox"
                                       value="1" <?php
                                if (config_item('front_slider') != '') {
                                    echo "checked";
                                } ?> />
                                <label for="ext_url" class="label-success"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('home_slider_speed') ?></label>

                        <div class="col-lg-6">
                            <div class="input-group">
                                <input type="text" value="<?php if (config_item('home_slider_speed') != '') {
                                    echo config_item('home_slider_speed');
                                } ?>" name="home_slider_speed" class="form-control">
                                <div class="input-group-addon"><?= lang('second') ?></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"> <?= lang('plan_expiry_date_alert') ?> </label>

                        <div class="col-lg-6">
                            <div class="input-group">
                                <input type="text" value="<?php if (config_item('plan_expiry_date_alert') != '') {
                                    echo config_item('plan_expiry_date_alert');
                                } else {
                                    echo 5;
                                } ?>" name="plan_expiry_date_alert" class="form-control">
                                <div class="input-group-addon">Days</div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
        <!-- End Form -->
    </div>
</div>
