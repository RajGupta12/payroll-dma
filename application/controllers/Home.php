<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('cms_menu_model');
        $this->load->model('cms_menuitems_model');
        $this->load->model('cms_frontend_model');

    }

    public function index()
    {
        $data['title'] = ucfirst('home');
        $data['active_menu'] = "home";
        $data['page_info'] = get_row('front_cms_pages', array('slug' => 'home'));
        $data['subview'] = $this->load->view('frontend/pages/index', $data, true);
        $this->load->view('frontend/_layout_front', $data);
    }

    public function page($slug = null)
    {
        $data['packege_id'] = $this->input->post('pricing_id', true);
        $data['page_info'] = get_row('front_cms_pages', array('slug' => $slug));

        $data['currency_wise_price'] = $this->get_currencywise_price(true);
        if (empty($data['packege_id'])) {
            $data['packege_id'] = $data['currency_wise_price'][0]->frontend_pricing_id;
        }
        if (!empty($data['page_info'])) {
            $data['active_menu'] = $data['page_info']->slug;
            $data['title'] = $data['page_info']->title;
            $data['subview'] = $this->load->view('frontend/pages/index', $data, true);
        } else {
            $data['subview'] = $this->load->view('frontend/pages/404_page', $data, true);
        }
        $this->load->view('frontend/_layout_front', $data);
    }

    function get_currencywise_price($frontend = null, $currency_price = null)
    {

        if (!empty($currency_price)) {
            $currencywise_price = $currency_price;
        } else {
            $currencywise_price = $this->input->post('currencywise_price', true);
            if (empty($currencywise_price)) {
                $currencywise_price = config_item('default_currency');
            }
        }

        $currency = get_row('tbl_currencies', array('code' => $currencywise_price));
        $all_pricing = $this->db->where('currency', $currencywise_price)->group_by('frontend_pricing_id')->get('tbl_currencywise_price')->result();

        $result = array();

        foreach ($all_pricing as $v_price) {
            $pricing_info = get_row('tbl_frontend_pricing', array('id' => $v_price->frontend_pricing_id));
            if (!empty($pricing_info)) {
                $v_price->name = $pricing_info->name;
                $v_price->currency = $currency->symbol;
                array_push($result, $v_price);
            }
        }
        if (!empty($frontend)) {
            return $result;
        } else {
            echo json_encode($result);
            exit();
        }
    }

    public function contact()
    {

        $data = $this->cms_frontend_model->array_from_post(array('name', 'email', 'phone', 'subject', 'description'));


        $this->cms_frontend_model->_table_name = 'tbl_front_contact_us';
        $this->cms_frontend_model->_primary_key = 'id';
        $id = $this->cms_frontend_model->save($data);


        $email_template = $this->cms_frontend_model->check_by(array('email_group' => 'contact_request_email'), 'tbl_email_templates');
        $message = $email_template->template_body;
        $subject = $email_template->subject;

        $title = str_replace("{NAME}", $data['name'], $message);
        $Link = str_replace("{LINK}", base_url() . 'admin/front/mailbox/view_email/' . $id . '/1', $title);
        $message = str_replace("{SITE_NAME}", config_item('company_name'), $Link);
        $data['message'] = $message;
        $message = $this->load->view('email_template', $data, TRUE);

        $params['subject'] = $subject;

        $params['resourceed_file'] = '';
        $params['message'] = $message;
        $all_users = all_admin();
        foreach ($all_users as $v_user) {
            $params['recipient'] = $v_user->email;
            $this->cms_frontend_model->send_email($params);
        }

        $type = "success";
        $message = 'Your message sent. Thanks for contacting. We will Contact you Soon.';

        echo json_encode(array('status' => $type, 'message' => $message));
        exit();
    }

    function subscribe()
    {
        $sbdata['email'] = $this->input->post('email');
        $sbdata['status'] = 1;
        $sbdata['ip'] = $this->input->ip_address();
        $sbdata['user_agent'] = $this->returnUserAgent();

        $this->cms_frontend_model->_table_name = 'tbl_subscribers';
        $this->cms_frontend_model->_primary_key = 'subscribers_id';
        $this->cms_frontend_model->save_data($sbdata);


        $type = "Success";
        $message = 'Subscribed Successfully.';

        $Response = array('status' => $type, 'message' => $message);
        echo json_encode($Response);
        exit;
    }

    public function returnUserAgent()
    {
        $this->load->library('user_agent');

        $agent = '';
        // is it browser? if yes get browser name and version
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } // robot?
        elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } // mobile ?
        elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }

        return $agent;
    }

}
