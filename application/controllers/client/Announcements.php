<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Announcements extends Client_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('announcements_model');

    }

    public function index($id = NULL)
    {

        $data['title'] = lang('all') . ' ' . lang('announcements');
        if ($id) {
            $data['announcements'] = $this->db->where('announcements_id', $id)->get('tbl_announcements')->row();
            if (empty($data['announcements'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('client/announcements');
            }
        }
        $data['all_announcements'] = $this->db->get('tbl_announcements')->result();

        $data['subview'] = $this->load->view('client/announcements/all_announcements', $data, TRUE);
        $this->load->view('client/_layout_main', $data); //page load
    }


    public function announcementsList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_announcements';
            $this->datatables->join_table = array('	tbl_account_details');
            $this->datatables->join_where = array('tbl_account_details.user_id=tbl_announcements.user_id');

            $custom_field = custom_form_table_search(16);
            $action_array = array('tbl_announcements.announcements_id');
            $main_column = array('title', 'description', 'start_date', 'end_date', 'tbl_account_details.fullname',);
            $result = array_merge($main_column, $custom_field, $action_array);
            $this->datatables->column_order = $result;
            $this->datatables->column_search = $result;

            $this->datatables->order = array('announcements_id' => 'desc');

            $fetch_data = make_datatables();

            $edited = can_action('100', 'edited');
            $deleted = can_action('100', 'deleted');
            $data = array();
            foreach ($fetch_data as $_key => $v_announcements) {
                $action = null;
                $sub_array = array();
                $sub_array[] = $v_announcements->title;
                $sub_array[] = fullname($v_announcements->user_id);
                $sub_array[] = display_date($v_announcements->start_date);
                $sub_array[] = display_date($v_announcements->end_date);
                $custom_form_table = custom_form_table(16, $v_announcements->announcements_id);
                if (!empty($custom_form_table)) {
                    foreach ($custom_form_table as $c_label => $v_fields) {
                        $sub_array[] = $v_fields;
                    }
                }
                if ($v_announcements->status == 'unpublished') {
                    $status = '<span class="label label-danger">' . lang('unpublished') . ' </span>';
                } else {
                    $status = '<span class="label label-success"> ' . lang('published') . ' </span>';
                }
                $sub_array[] = $status;

                $action .= btn_view_modal('client/announcements/announcements_details/' . $v_announcements->announcements_id) . ' ';

                $sub_array[] = $action;
                $data[] = $sub_array;
            }

            render_table($data);
        } else {
            redirect('client/dashboard');
        }
    }

    public function download_files($id, $fileName)
    {
        $appl_info = $this->announcements_model->check_by(array('announcements_id' => $id), 'tbl_announcements');
        if (empty($appl_info)) {
            $type = "error";
            $message = "No Record Found";
            set_message($type, $message);
            redirect('client/announcements');
        }
        $this->load->helper('download');
        if (!empty($appl_info->attachment)) {
            $down_data = file_get_contents('uploads/' . $fileName); // Read the file's contents
            force_download($fileName, $down_data);
        }
    }

    public function announcements_details($id)
    {
        $data['title'] = lang('announcements_details'); //Page title

        $this->announcements_model->_table_name = "tbl_announcements"; // table name
        $this->announcements_model->_order_by = "announcements_id"; // $id
        $data['announcements_details'] = $this->announcements_model->get_by(array('announcements_id' => $id), TRUE);
        if (empty($data['announcements_details'])) {
            $type = "error";
            $message = "No Record Found";
            set_message($type, $message);
            redirect('client/announcements');
        }
        $this->announcements_model->_primary_key = 'announcements_id';
        $updata['view_status'] = '1';
        $this->announcements_model->save($updata, $id);
        $data['subview'] = $this->load->view('client/announcements/announcements_details', $data, FALSE);
        $this->load->view('client/_layout_modal_lg', $data); //page load
    }

    public function view_announcements_details($id)
    {
        $data['title'] = lang('announcements_details'); //Page title

        $this->announcements_model->_table_name = "tbl_announcements"; // table name
        $this->announcements_model->_order_by = "announcements_id"; // $id
        $data['announcements_details'] = $this->announcements_model->get_by(array('announcements_id' => $id), TRUE);
        if (empty($data['announcements_details'])) {
            $type = "error";
            $message = "No Record Found";
            set_message($type, $message);
            redirect('client/announcements');
        }
        $this->announcements_model->_primary_key = 'announcements_id';
        $updata['view_status'] = '1';
        $this->announcements_model->save($updata, $id);
        $data['subview'] = $this->load->view('client/announcements/announcements_details', $data, TRUE);
        $this->load->view('client/_layout_main', $data); //page load
    }


}
