<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Frontend extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('account_model');
        $super_admin = super_admin();
        if (empty($super_admin)) {
            redirect('404');
        }
    }

    public function home()
    {
        $data['title'] = lang('home');
        $data['subview'] = $this->load->view('admin/frontend/home', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function subscriber()
    {
        $data['title'] = lang('home');
        $data['subview'] = $this->load->view('admin/frontend/subscriber', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function subscriberList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_subscribers';
            $main_column = array('subscribers_id', 'email');
            $action_array = array('subscribers_id');
            $result = array_merge($main_column, $action_array);
            $this->datatables->column_order = $result;
            $this->datatables->column_search = $result;
            $this->datatables->order = array('subscribers_id' => 'desc');


            $fetch_data = make_datatables();
            $data = array();

            foreach ($fetch_data as $_key => $v_subscriber) {
                if (!empty($v_subscriber)) {
                    $action = null;
                    $sub_array = array();

                    if ($v_subscriber->status == '1') {
                        $label = 'success';
                        $status = lang('subscribed');
                        $type = 0;
                        $type_label = 'warning';
                        $type_text = lang('un_subscribed');
                    } else {
                        $label = 'warning';
                        $status = lang('un_subscribed');
                        $type = 1;
                        $type_label = 'success';
                        $type_text = lang('subscribed');
                    }


                    $sub_array[] = $v_subscriber->email;
                    $sub_array[] = '<span class="badge badge-' . $label . '">' . $status . '</span>';
                    $sub_array[] = $v_subscriber->ip;
                    $sub_array[] = $v_subscriber->user_agent;
                    $sub_array[] = display_datetime($v_subscriber->created_at);

                    $action .= '<a data-toggle="tooltip" data-placement="top"
                               class="btn btn-' . $type_label . ' btn-xs"
                               title="Click to ' . $type_text . '"
                               href="' . base_url("admin/frontend/subscriber_action/$type/$v_subscriber->subscribers_id") . '"><span
                                    class="fa fa-' . (($v_subscriber->status == 0) ? "check" : "times") . '"></span></a>' . ' ';

                    $action .= ajax_anchor(base_url("admin/frontend/delete/tbl_subscribers/$v_subscriber->subscribers_id"), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key)) . ' ';

                    $sub_array[] = $action;
                    $data[] = $sub_array;
                }

            }
            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function subscriptions()
    {
        $data['title'] = lang('home');
        $data['subview'] = $this->load->view('admin/frontend/subscriptions', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function subscriptions_details($id)
    {

        $data['title'] = lang('home');
        $data['subscription_info'] = get_row('tbl_subscriptions', array('subscriptions_id' => $id));
        $data['subview'] = $this->load->view('admin/frontend/subscriptions_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function subscriptionsList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_subscriptions';
            $main_column = array('subscriptions_id', 'status');
            $action_array = array('subscriptions_id');
            $result = array_merge($main_column, $action_array);
            $this->datatables->column_order = $result;
            $this->datatables->column_search = $result;
            $this->datatables->order = array('subscriptions_id' => 'desc');

            $where = array();

            $filterBy = explode('/', $_SERVER['HTTP_REFERER']);
            if (!empty($filterBy[7])) {
                $where = array('status' => $filterBy[7]);
            }

            $fetch_data = make_datatables($where);
            $data = array();

            foreach ($fetch_data as $_key => $v_subscriptions) {
                if (!empty($v_subscriptions)) {
                    $action = null;
                    $sub_array = array();


                    if (empty($v_subscriptions->currency)) {
                        $currency_code = config_item('default_currency');
                    } else {
                        $currency_code = $v_subscriptions->currency;
                    }

                    $currency = $this->account_model->check_by(array('code' => $currency_code), 'tbl_currencies');
                    if ($v_subscriptions->status == 'pending') {
                        $label = 'primary';
                    } else if ($v_subscriptions->status == 'running') {
                        $label = 'success';
                    } else if ($v_subscriptions->status == 'expired') {
                        $label = 'warning';
                    } else {
                        $label = 'danger';
                    }

                    $sub_array[] = '<a data-toggle="modal" data-target="#myModal"
                               href="' . base_url("admin/global_controller/package_details/$v_subscriptions->pricing_id") . '"
                               class="text-center">' . $this->db->where(array('id' => $v_subscriptions->pricing_id))->get('tbl_frontend_pricing')->row()->name . '</a>';

                    $sub_array[] = $v_subscriptions->trial_period . ' ' . lang('days');
                    $sub_array[] = $currency->name . '(' . $currency->symbol . ')';
                    $sub_array[] = lang($v_subscriptions->frequency);
                    $sub_array[] = '<span class="label label-' . $label . '">' . lang($v_subscriptions->status) . '</span>';
                    $sub_array[] = strftime(config_item('date_format'), strtotime($v_subscriptions->created_date));


                    if ($v_subscriptions->status == 'pending') {
                        $action .= '<a data-toggle="tooltip" data-placement="top" class="btn btn-success btn-xs"
                                   title="' . lang('send_activation_token') . '"
                                   href="' . base_url("admin/frontend/send_activation_token/. $v_subscriptions->subscriptions_id") . '" ><span class="fa fa-envelope-o"></span></a>' . ' ';
                    }
                    $action .= ajax_anchor(base_url("admin/frontend/delete_subscriptions/$v_subscriptions->subscriptions_id"), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key)) . ' ';

                    $action .= btn_view(base_url('admin/frontend/subscriptions_details/' . $v_subscriptions->subscriptions_id)) . ' ';

                    $sub_array[] = $action;
                    $data[] = $sub_array;
                }

            }
            render_table($data, $where);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function send_activation_token($id)
    {
        $subscription_info = get_row('tbl_subscriptions', array('subscriptions_id' => $id));
        if (!empty($subscription_info) && $subscription_info->status == 'pending') {
            $login_data['activation_period'] = config_item('email_activation_expire') / 3600;
            $this->send_token_activation_email($subscription_info);
        }
        redirect('admin/frontend/subscriptions');
    }

    function send_token_activation_email($subs_info)
    {
        $activation_period = config_item('email_activation_expire') / 3600;
        $sub_domain = (isset($_SERVER['HTTPS']) ? "https://" : "http://");
        $sub_domain .= $subs_info->domain . '.';
        $sub_domain .= $_SERVER['HTTP_HOST'];
        $sub_domain .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

        $email_template = $this->account_model->check_by(array('email_group' => 'token_activate_account'), 'tbl_email_templates');

        $activatation_token = str_replace("{ACTIVATION_TOKEN}", $subs_info->activation_tocken, $email_template->template_body);
        $activate_url = str_replace("{ACTIVATE_URL}", $sub_domain, $activatation_token);
        $activate_period = str_replace("{ACTIVATION_PERIOD}", $activation_period, $activate_url);
        $message = str_replace("{SITE_NAME}", config_item('company_name'), $activate_period);

        $params['recipient'] = $subs_info->email;
        $params['subject'] = '[ ' . config_item('company_name') . ' ]' . ' ' . $email_template->subject;
        $params['message'] = $message;
        $params['resourceed_file'] = '';
        $this->account_model->send_email($params);
    }

    public function update_sub_validity($id)
    {
        $validity = $this->input->post('validity', true);
        $status = $this->input->post('status', true);
        $data = $this->total_count_date($id, $validity, $status);
        if (empty($data['status'])) {
            $data['status'] = $status;
        }

        $data['remarks'] = $this->input->post('remarks', true);

        $this->account_model->_table_name = "tbl_subscriptions"; // table name
        $this->account_model->_primary_key = "subscriptions_id"; // $id
        $this->account_model->save($data, $id);

        set_message('success', lang('update_settings'));
        redirect('admin/frontend/subscriptions_details/' . $id);

    }

    public function delete_subscriptions($id)
    {
        $action = 'activity_delete_companies';
        $msg = lang('delete_message');

        $sub_info = get_row('tbl_subscriptions', array('subscriptions_id' => $id));
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'subscriptions',
            'module_field_id' => $id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $sub_info->name
        );
        $this->account_model->_table_name = 'tbl_activities';
        $this->account_model->_primary_key = 'activities_id';
        $this->account_model->save($activity);

        $user_info = $this->db->where(array('companies_id' => $id))->get('tbl_users')->result();
        if (!empty($user_info)) {
            foreach ($user_info as $v_user) {
                $this->account_model->_table_name = "tbl_account_details"; //table name
                $this->account_model->delete_multiple(array('user_id' => $v_user->user_id));
            }
            $this->account_model->_table_name = "tbl_users"; //table name
            $this->account_model->delete_multiple(array('companies_id' => $id));
        }

        $this->account_model->_table_name = "tbl_config"; //table name
        $this->account_model->delete_multiple(array('companies_id' => $id));

        $this->account_model->_table_name = "tbl_working_days"; //table name
        $this->account_model->delete_multiple(array('companies_id' => $id));

        $this->account_model->_table_name = "tbl_menu"; //table name
        $this->account_model->delete_multiple(array('companies_id' => $id));

        $this->account_model->_table_name = "tbl_dashboard"; //table name
        $this->account_model->delete_multiple(array('companies_id' => $id));

        $this->account_model->_table_name = "tbl_client_menu"; //table name
        $this->account_model->delete_multiple(array('companies_id' => $id));

        $this->account_model->_table_name = "tbl_email_templates"; //table name
        $this->account_model->delete_multiple(array('companies_id' => $id));


        // *********** Delete into tbl_salary_allowance *******************
        $subscriptions_history = get_result('tbl_subscriptions_history', array('subscriptions_id' => $id));
        if (!empty($subscriptions_history)) {
            foreach ($subscriptions_history as $v_history) {
                // *********** Delete into tbl_salary_allowance *******************
                $this->account_model->_table_name = "tbl_subscription_payment"; // table name
                $this->account_model->delete_multiple(array('subscriptions_history_id' => $v_history->id));

            }
        }
        $this->account_model->_table_name = "tbl_subscriptions_history"; // table name
        $this->account_model->delete_multiple(array('subscriptions_id' => $id));


        // *********** Delete into tbl_salary_template *******************
        $this->account_model->_table_name = "tbl_subscriptions"; // table name
        $this->account_model->_primary_key = "subscriptions_id"; // $id
        $this->account_model->delete($id);

        $type = "success";
        echo json_encode(array("status" => $type, 'message' => $msg));
        exit();
    }



    function total_count_date($id, $date, $status)
    {

        $sub_info = get_row('tbl_subscriptions', array('subscriptions_id' => $id));
        $time = date('Y-m-d', strtotime($sub_info->created_date));
        $to_date = strtotime($time); //past date.
        $cur_date = strtotime($date);
        $timeleft = $cur_date - $to_date;
        $daysleft = round((($timeleft / 24) / 60) / 60);
        if ($date > $time && $status == 'expired') {
            $data['status'] = 'running';
        }
        if ($sub_info->trial_period != 0) {
            $data['trial_period'] = $daysleft;
            $data['is_trial'] = 'Yes';
        } else {
            $data['trial_period'] = 0;
            $data['is_trial'] = 'No';
            $data['expired_date'] = date("Y-m-d", strtotime($daysleft . "day"));
        }
        return $data;
    }


    public function subscriber_action($type, $id)
    {

        $data['status'] = $type;

        $this->account_model->_table_name = "tbl_subscribers"; // table name
        $this->account_model->_primary_key = "subscribers_id"; // $id
        $this->account_model->save($data, $id);

        set_message('success', lang('update_settings'));
        redirect('admin/frontend/subscriber/' . $id);

    }


    public function save_menu($d_id = null, $flag = null)
    {
        $report_menu_id = json_decode($this->input->post('frontend_menu', true));
        if (!empty($report_menu_id)) {
            foreach ($report_menu_id as $mrkey => $r_id) {
                $rdata['sort'] = $mrkey + 1;
                $this->account_model->_table_name = 'tbl_frontend_menu';
                $this->account_model->_primary_key = 'menu_id';
                $this->account_model->save($rdata, $r_id);
            }
        }
        if (!empty($d_id)) {
            $where = array('menu_id' => $d_id);
            $action = array('status' => $flag);
            $this->account_model->set_action($where, $action, 'tbl_frontend_menu');
        }
        $type = "success";
        $message = lang('update_settings');
        echo json_encode(array('status' => $type, 'message' => $message));
        exit();
    }

    public function new_slider($id = null)
    {
        $data['title'] = lang('new_slider');
        /// edit and update get employee award info
        if (!empty($id)) {
            $data['slider_info'] = $this->account_model->check_by(array('id' => $id), 'tbl_frontend_slider');
        }
        $data['subview'] = $this->load->view('admin/frontend/new_slider', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_slider($id = null)
    {
        $data = $this->account_model->array_from_post(array('title', 'description', 'button_one', 'button_one_link', 'button_two', 'button_two_link', 'button_two_link', 'status'));

        if (!empty($_FILES['slider']['name'])) {
            $val = $this->account_model->uploadImage('slider');
            $val == TRUE || redirect('admin/frontend/home');
            $data['slider'] = $val['path'];
        }

        $this->account_model->_table_name = "tbl_frontend_slider"; // table name
        $this->account_model->_primary_key = "id"; // $id
        $this->account_model->save($data, $id);

        set_message('success', lang('update_settings'));
        redirect('admin/frontend/home');
    }

    function change_status($tbl, $id = null, $status = null)
    {
        if (!empty($id)) {
            $where = array('id' => $id);
            $action = array('status' => $status);
            $this->account_model->set_action($where, $action, $tbl);
        }
        $type = "success";
        $message = lang('update_settings');
        echo json_encode(array('status' => $type, 'message' => $message));
        exit();
    }

    public function features($id = NULL)
    {
        $data['title'] = lang('home');
        if ($id) {
            $data['active'] = 2;
            $can_edit = $this->account_model->can_action('tbl_accounts', 'edit', array('account_id' => $id));
            $edited = can_action('36', 'edited');
            if (!empty($can_edit) && !empty($edited)) {
                $data['account_info'] = $this->account_model->check_by(array('account_id' => $id), 'tbl_accounts');
            }
        } else {
            $data['active'] = 1;
        }
        $data['subview'] = $this->load->view('admin/frontend/home', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function new_features($id = null)
    {
        $data['title'] = lang('new_slider');
        /// edit and update get employee award info
        if (!empty($id)) {
            if (is_numeric($id)) {
                $data['features_info'] = $this->account_model->check_by(array('id' => $id), 'tbl_frontend_features');
            } else {
                $data['type'] = $id;
            }
        }
        $data['subview'] = $this->load->view('admin/frontend/new_features', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_features($id = null)
    {
        $features_id = json_decode($this->input->post('features', true));
        if (!empty($features_id)) {
            foreach ($features_id as $mrkey => $r_id) {
                $rdata['sort'] = $mrkey + 1;
                $this->account_model->_table_name = 'tbl_frontend_features';
                $this->account_model->_primary_key = 'id';
                $this->account_model->save($rdata, $r_id);
            }
        }
        $data = $this->account_model->array_from_post(array('title', 'description', 'icon', 'status'));
        if (!empty($data['title'])) {
            $type = $this->input->post('type', true);
            if (!empty($type)) {
                $data['type'] = $type;
            }
            if (!empty($_FILES['icon']['name'])) {
                $val = $this->account_model->uploadImage('icon');
                $val == TRUE || redirect('admin/frontend/home');
                $data['icon'] = $val['path'];
            }
            $this->account_model->_table_name = "tbl_frontend_features"; // table name
            $this->account_model->_primary_key = "id"; // $id
            $this->account_model->save($data, $id);
        }
        set_message('success', lang('update_settings'));
        redirect('admin/frontend/home');
    }

    public function delete($tbl, $id)
    {
        $field = 'id';
        if ($tbl == 'tbl_subscribers') {
            $field = 'subscribers_id';
        }
        $this->account_model->_table_name = $tbl;
        $this->account_model->_primary_key = $field;
        $this->account_model->delete($id);

        $type = "success";
//            $message = $msg;
//            set_message($type, $message);
        echo json_encode(array("status" => $type, 'message' => 'deleted'));
        exit();
//        redirect('admin/account/manage_account');
    }

    public function pricing($id = null)
    {
        $data['title'] = lang('pricing');


        if (!empty($id) && $id != 'active' && $id != 'inactive') {
            $data['active'] = 2;
            $data['pricing_info'] = $this->account_model->check_by(array('id' => $id), 'tbl_frontend_pricing');
        } else {
            $data['active'] = 1;
        }
        $data['subview'] = $this->load->view('admin/frontend/pricing', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function pricingList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_frontend_pricing';
            $main_column = array('sort', 'name', 'status');
            $action_array = array('sort');
            $result = array_merge($main_column, $action_array);
            $this->datatables->column_order = $result;
            $this->datatables->column_search = $result;
            $this->datatables->order = array('sort' => 'asc');

            $where = array();

            $filterBy = explode('/', $_SERVER['HTTP_REFERER']);


            if (!empty($filterBy[7]) && $filterBy[7] == 'active') {
                $where = array('status' => 1);
            } elseif (!empty($filterBy[7]) && $filterBy[7] == 'inactive') {
                $where = array('status' => 0);
            }

            $fetch_data = make_datatables($where);

            $data = array();

            foreach ($fetch_data as $_key => $v_pricing) {
                if (!empty($v_pricing)) {
                    $action = null;
                    $sub_array = array();

                    $sub_array[] = $v_pricing->name;
                    $sub_array[] = $v_pricing->trial_period . ' ' . lang('days');
                    $sub_array[] = $v_pricing->recommended;
                    $sub_array[] = '<div class="checkbox ajax_change_status">
                                        <input data-id="' . $v_pricing->id . '" data-toggle="toggle"
                                               name="status"
                                               value="1" ' . ($v_pricing->status == 1 ? "checked" : '') . ' data-on="' . lang('yes') . '" data-off="' . lang('no') . '"
                                               data-onstyle="success btn-xs"
                                               data-offstyle="danger btn-xs" type="checkbox"
                                     </div>';


                    $action .= btn_edit('admin/frontend/pricing/' . $v_pricing->id) . ' ';
                    $action .= btn_view_modal(base_url('admin/global_controller/package_details/' . $v_pricing->id)) . ' ';

                    $action .= ajax_anchor(base_url("admin/frontend/delete/tbl_frontend_pricing/$v_pricing->id"), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key)) . ' ';


                    $sub_array[] = $action;
                    $data[] = $sub_array;
                }

            }
            render_table($data, $where);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function coupon($id = null)
    {
        $data['title'] = lang('coupon');
        if (!empty($id)) {
            $data['active'] = 2;
            $data['coupon_info'] = $this->account_model->check_by(array('id' => $id), 'tbl_frontend_coupon');
        } else {
            $data['active'] = 1;
        }
        $data['subview'] = $this->load->view('admin/frontend/coupon', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function resources($id = null)
    {
        $data['title'] = lang('resources');
        $data['subview'] = $this->load->view('admin/frontend/resources', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_pricing($id = null)
    {

        $features_id = json_decode($this->input->post('pricing', true));
        if (!empty($features_id)) {
            foreach ($features_id as $mrkey => $r_id) {
                $rdata['sort'] = $mrkey + 1;
                $this->account_model->_table_name = 'tbl_frontend_pricing';
                $this->account_model->_primary_key = 'id';
                $this->account_model->save($rdata, $r_id);
            }
        }

        $data = $this->account_model->array_from_post(array('name', 'amount', 'purchase_no', 'supplier_no',
            'employee_no', 'client_no', 'leads', 'accounting', 'bank_account', 'tasks', 'project_no', 'disk_space', 'invoice_no', 'trial_period'
        , 'description', 'status', 'recommended'));

        $data['online_payment'] = ($this->input->post('online_payment') == 'Yes') ? 'Yes' : 'No';
        $data['calendar'] = ($this->input->post('calendar') == 'Yes') ? 'Yes' : 'No';
        $data['mailbox'] = ($this->input->post('mailbox') == 'Yes') ? 'Yes' : 'No';
        $data['live_chat'] = ($this->input->post('live_chat') == 'Yes') ? 'Yes' : 'No';
        $data['tickets'] = ($this->input->post('tickets') == 'Yes') ? 'Yes' : 'No';
        $data['filemanager'] = ($this->input->post('filemanager') == 'Yes') ? 'Yes' : 'No';
        $data['recruitment'] = ($this->input->post('recruitment') == 'Yes') ? 'Yes' : 'No';
        $data['attendance'] = ($this->input->post('attendance') == 'Yes') ? 'Yes' : 'No';
        $data['payroll'] = ($this->input->post('payroll') == 'Yes') ? 'Yes' : 'No';
        $data['leave_management'] = ($this->input->post('leave_management') == 'Yes') ? 'Yes' : 'No';
        $data['performance'] = ($this->input->post('performance') == 'Yes') ? 'Yes' : 'No';
        $data['training'] = ($this->input->post('training') == 'Yes') ? 'Yes' : 'No';
        $data['reports'] = ($this->input->post('reports') == 'Yes') ? 'Yes' : 'No';
        $data['allow_paypal'] = ($this->input->post('allow_paypal') == 'Yes') ? 'Yes' : 'No';
        $data['allow_stripe'] = ($this->input->post('allow_stripe') == 'Yes') ? 'Yes' : 'No';
        $data['allow_2checkout'] = ($this->input->post('allow_2checkout') == 'Yes') ? 'Yes' : 'No';
        $data['allow_authorize'] = ($this->input->post('allow_authorize') == 'Yes') ? 'Yes' : 'No';
        $data['allow_ccavenue'] = ($this->input->post('allow_ccavenue') == 'Yes') ? 'Yes' : 'No';
        $data['allow_braintree'] = ($this->input->post('allow_braintree') == 'Yes') ? 'Yes' : 'No';
        $data['allow_mollie'] = ($this->input->post('allow_mollie') == 'Yes') ? 'Yes' : 'No';
        $data['allow_payumoney'] = ($this->input->post('allow_payumoney') == 'Yes') ? 'Yes' : 'No';

        $this->account_model->_table_name = "tbl_frontend_pricing"; // table name
        $this->account_model->_primary_key = "id"; // $id
        if (!empty($id)) { // if id exist in db update data
            $check_id = array('id !=' => $id);
        } else { // if id is not exist then set id as null
            $check_id = null;
        }
        $where = array('recommended' => 'Yes');
        $already_recommended = $this->account_model->check_update('tbl_frontend_pricing', $where, $check_id);
        if (!empty($already_recommended)) {
            foreach ($already_recommended as $v_recommended) {
                $udata['recommended'] = 'No';
                $this->account_model->save($udata, $v_recommended->id);
            }
        }
        if (empty($data['recommended'])) {
            $data['recommended'] = 'No';
        }

        if (!empty($data['name'])) {
            if (empty($data['status']) && $data['status'] != 0) {
                $data['status'] = 1;
            }
            $id = $this->account_model->save($data, $id);
        }
        $this->account_model->_table_name = 'tbl_currencywise_price';
        $this->account_model->_primary_key = 'currencywise_price_id';
        if (!empty($id)) {
            $this->account_model->delete_multiple(array('frontend_pricing_id' => $id));

            $currency = $this->input->post('currency', true);
            $monthly = $this->input->post('monthly', true);
            $yearly = $this->input->post('yearly', true);

            foreach ($currency as $key => $cur) {
                if (!empty($monthly[$key]) || !empty($yearly[$key])) {
                    $cr_data['frontend_pricing_id'] = $id;
                    $cr_data['currency'] = $cur;
                    $cr_data['monthly'] = $monthly[$key];
                    $cr_data['yearly'] = $yearly[$key];

                    $this->account_model->save($cr_data);
                }
            }
        }
        set_message('success', lang('update_settings'));
        redirect('admin/frontend/pricing');
    }

    public
    function save_coupon($id = null)
    {
        $data = $this->account_model->array_from_post(array('name', 'code', 'amount', 'type', 'end_date', 'pricing_id', 'show_on_pricing'));
        if (!empty($id)) { // if id exist in db update data
            $check_id = array('id !=' => $id);
        } else { // if id is not exist then set id as null
            $check_id = null;
        }
        if (!empty($data['pricing_id'])) {
            $where = array('pricing_id' => $data['pricing_id'], 'show_on_pricing' => 'Yes');
            $already_show = $this->account_model->check_update('tbl_frontend_coupon', $where, $check_id);
            $this->account_model->_table_name = "tbl_frontend_coupon"; // table name
            $this->account_model->_primary_key = "id"; // $id
            if (!empty($already_show)) {
                foreach ($already_show as $v_show) {
                    $udata['show_on_pricing'] = 'No';
                    $this->account_model->save($udata, $v_show->id);
                }
            }
        } else {
            $data['pricing_id'] = 0;
        }
        if (empty($data['show_on_pricing'])) {
            $data['show_on_pricing'] = 'No';
        }
        if (!empty($data['name'])) {
            if (empty($data['status'])) {
                $data['status'] = 1;
            }
            $this->account_model->_table_name = "tbl_frontend_coupon"; // table name
            $this->account_model->_primary_key = "id"; // $id
            $this->account_model->save($data, $id);
        }
        set_message('success', lang('update_settings'));
        redirect('admin/frontend/coupon');
    }

    function saved_config()
    {
        if (!empty($_POST)) {
            foreach ($_POST as $key => $value) {
                $data = array('value' => $value);
                $this->db->where('config_key', $key)->update('tbl_config', $data);
                $exists = $this->db->where('config_key', $key)->get('tbl_config');
                if ($exists->num_rows() == 0) {
                    $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
                }
            }
        }
        set_message('success', lang('update_settings'));
        redirect('admin/frontend/home');
    }

    function saved_config_image()
    {
        if (!empty($_FILES['fully_management_system']['name'])) {
            $val = $this->account_model->uploadImage('fully_management_system');
            $val == TRUE || redirect('admin/frontend/home');
            $input_data['fully_management_system'] = $val['path'];
        }
        if (!empty($input_data)) {
            foreach ($input_data as $key => $value) {
                $data = array('value' => $value);
                $this->db->where('config_key', $key)->update('tbl_config', $data);
                $exists = $this->db->where('config_key', $key)->get('tbl_config');
                if ($exists->num_rows() == 0) {
                    $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
                }
            }
        }
        set_message('success', lang('update_settings'));
        redirect('admin/frontend/home');
    }

    public function new_ratings($id = null)
    {
        $data['title'] = lang('new_ratings');
        /// edit and update get employee award info
        if (!empty($id)) {
            $data['ratings_info'] = $this->account_model->check_by(array('id' => $id), 'tbl_customer_ratings');
        }
        $data['subview'] = $this->load->view('admin/frontend/new_ratings', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }


    public
    function save_ratings($id = null)
    {

        $data = $this->account_model->array_from_post(array('name', 'position', 'description', 'ratings', 'status'));

        if (!empty($data['name'])) {

            if (!empty($_FILES['image']['name'])) {
                $val = $this->account_model->uploadImage('image');
                $val == TRUE || redirect('admin/frontend/home');
                $data['image'] = $val['path'];
            }
            $this->account_model->_table_name = "tbl_customer_ratings"; // table name
            $this->account_model->_primary_key = "id"; // $id
            $this->account_model->save($data, $id);
        }
        set_message('success', lang('update_settings'));
        redirect('admin/frontend/home');
    }

    public function quote_request()
    {
        $data['title'] = lang('quote_request');
        $data['subview'] = $this->load->view('admin/frontend/quote_request', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function partners($id = null)
    {
        $data['title'] = lang('partners');
        if (!empty($id)) {
            $data['active'] = 2;
            $data['partner_info'] = $this->account_model->check_by(array('id' => $id), 'tbl_partners');
        } else {
            $data['active'] = 1;
        }
        $data['subview'] = $this->load->view('admin/frontend/partners', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public
    function save_partners($id = null)
    {
        $data = $this->account_model->array_from_post(array('name', 'designation', 'facebook', 'twitter', 'linkedin', 'dribbble', 'description'));
        if (!empty($data['name'])) {
            if (!empty($_FILES['profile']['name'])) {
                $val = $this->account_model->uploadImage('profile');
                $val == TRUE || redirect('admin/frontend/home');
                $data['profile'] = $val['path'];
            }
            if (empty($data['status'])) {
                $data['status'] = 1;
            }
            $this->account_model->_table_name = "tbl_partners"; // table name
            $this->account_model->_primary_key = "id"; // $id
            $this->account_model->save($data, $id);
        }
        set_message('success', lang('update_settings'));
        redirect('admin/frontend/partners');
    }

    public function team($id = null)
    {
        $data['title'] = lang('our_team');
        if (!empty($id)) {
            $data['active'] = 2;
            $data['partner_info'] = $this->account_model->check_by(array('id' => $id), 'tbl_partners');
        } else {
            $data['active'] = 1;
        }
        $data['subview'] = $this->load->view('admin/frontend/team', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public
    function save_team($id = null)
    {
        $data = $this->account_model->array_from_post(array('name', 'type', 'designation', 'facebook', 'twitter', 'linkedin', 'dribbble', 'description'));
        if (!empty($data['name'])) {
            if (!empty($_FILES['profile']['name'])) {
                $val = $this->account_model->uploadImage('profile');
                $val == TRUE || redirect('admin/frontend/home');
                $data['profile'] = $val['path'];
            }
            if (empty($data['status'])) {
                $data['status'] = 1;
            }
            $this->account_model->_table_name = "tbl_partners"; // table name
            $this->account_model->_primary_key = "id"; // $id
            $this->account_model->save($data, $id);
        }
        set_message('success', lang('update_settings'));
        redirect('admin/frontend/team');
    }


}
