<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('cms_frontend_model');
        $this->load->model('settings_model');
        $this->load->helper('ckeditor');
        if (empty(super_admin())) {
            redirect('404');
        }
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );

        $this->language_files = $this->settings_model->all_files();

    }

    public function index()
    {
        $data['title'] = lang('general_settings');
        $data['page'] = lang('settings');
        $data['load_setting'] = 'general';
        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function theme()
    {
        $data['title'] = lang('theme_settings');
        $data['page'] = lang('settings');
        $data['load_setting'] = 'theme';
        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_theme()
    {
        $input_data = $this->settings_model->array_from_post(array('company_name', 'logo_or_icon', 'sidebar_theme', 'aside-float', 'aside-collapsed', 'layout-boxed', 'layout-fixed', 'login_position', 'RTL',));

        if (empty($input_data['active_custom_color'])) {
            $input_data['active_custom_color'] = '0';
        }
        if (empty($input_data['RTL'])) {
            $input_data['RTL'] = 0;
        }
        //logo Process
        if (!empty($_FILES['company_logo']['name'])) {
            $val = $this->settings_model->uploadImage('company_logo');
            $val == TRUE || redirect('admin/front/settings/theme');
            $input_data['company_logo'] = $val['path'];
        }
        //favicon Process
        if (!empty($_FILES['favicon']['name'])) {
            $val = $this->settings_model->uploadImage('favicon');
            $val == TRUE || redirect('admin/front/settings/theme');
            $input_data['favicon'] = $val['path'];
        }
        if (!empty($_FILES['login_background']['name'])) {
            $val = $this->settings_model->uploadImage('login_background');
            $val == TRUE || redirect('admin/front/settings/theme');
            $input_data['login_background'] = $val['path'];
        }

        $companies_id = $this->session->userdata('companies_id');
        if (empty($companies_id)) {
            $companies_id = null;
        }

        foreach ($input_data as $key => $value) {
            $where = array('companies_id' => $companies_id, 'config_key' => $key);
            $data = array('value' => $value);
            $this->db->where($where)->update('tbl_config', $data);
            $exists = $this->db->where($where)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("companies_id" => $companies_id, "config_key" => $key, "value" => $value));
            }
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => ('activity_save_theme_settings'),
            'value1' => $input_data['website_name']
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_theme_settings');
        set_message($type, $message);
        redirect('admin/front/settings/theme');
    }

    public function system()
    {
        $data['title'] = lang('system_settings');
        $data['page'] = lang('settings');

        $data['languages'] = $this->settings_model->get_active_languages();
        // get all timezone
        $data['timezones'] = $this->settings_model->timezones();
        // get all currencies
        $this->settings_model->_table_name = 'tbl_currencies';
        $this->settings_model->_order_by = 'name';
        $data['currencies'] = $this->settings_model->get();

        $data['load_setting'] = 'system';
        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_system()
    {
        $input_data = $this->settings_model->array_from_post(array('default_language', 'timezone', 'default_currency', 'company_country', 'enable_languages'));
        $companies_id = $this->session->userdata('companies_id');
        if (empty($companies_id)) {
            $companies_id = null;
        }
        foreach ($input_data as $key => $value) {
            $data = array('value' => $value);
            $where = array('companies_id' => $companies_id, 'config_key' => $key);
            $this->db->where($where)->update('tbl_config', $data);
            $exists = $this->db->where($where)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("companies_id" => $companies_id, "config_key" => $key, "value" => $value));
            }
        }

        // messages for user
        $type = "success";
        $message = lang('save_system_settings');
        set_message($type, $message);
        redirect('admin/front/settings/system');
    }

    public function email()
    {
        $data['page'] = lang('settings');
        $data['load_setting'] = 'email_settings';
        $data['title'] = lang('email_settings'); //Page title
        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function update_email()
    {
        $input_data = $this->settings_model->array_from_post(array('company_email', 'use_postmark',
            'postmark_api_key', 'postmark_from_address', 'protocol', 'smtp_host', 'smtp_user', 'smtp_port', 'smtp_encryption'));

        $smtp_pass = $this->input->post('smtp_pass', true);

        $companies_id = $this->session->userdata('companies_id');
        foreach ($input_data as $key => $value) {
            if (strtolower($value) == 'on') {
                $value = 'TRUE';
            } elseif (strtolower($value) == 'off') {
                $value = 'FALSE';
            }
            $data = array('value' => $value);
            $where = array('companies_id' => $companies_id, 'config_key' => $key);
            $this->db->where($where)->update('tbl_config', $data);
            $exists = $this->db->where($where)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("companies_id" => $companies_id, "config_key" => $key, "value" => $value));
            }
        }
        $smtp_pass = $this->input->post('smtp_pass', true);
        if (!empty($smtp_pass)) {
            $smtp_data = array('value' => encrypt($smtp_pass));
            $where = array('companies_id' => $companies_id, 'config_key' => 'smtp_pass');
            $this->db->where($where)->update('tbl_config', $smtp_data);
            $exists = $this->db->where($where)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("companies_id" => $companies_id, "config_key" => 'smtp_pass', "value" => $smtp_data));
            }
        }

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => ('activity_save_email_settings'),
            'value1' => $input_data['company_email']
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_email_settings');
        set_message($type, $message);
        redirect('admin/front/settings/email');
    }

    public function sent_test_email()
    {
        $test_email = $this->input->post('test_email', true);
        if (!empty($test_email)) {
            $params['subject'] = 'SMTP Setup Testing';
            $params['message'] = 'This is test SMTP email. <br />If you received this message that means that your SMTP settings is Corrects.';
            $params['recipient'] = $test_email;
            $params['resourceed_file'] = '';
            $result = $this->settings_model->send_email($params, true);
            if ($result == true) {
                set_message('success', 'Seems like your SMTP settings is Corrects. Check your email now. :)');
            } else {
                $s_data['email_error'] = '<h1>Your SMTP settings are not set correctly here is the debug log.</h1><br />' . show_error($this->email->print_debugger());
                $this->session->set_userdate($s_data);
            }
        }
        redirect('admin/front/settings/email');
    }

    public function update_profile()
    {
        $data['title'] = lang('update_profile');
        $data['subview'] = $this->load->view('admin/front/settings/update_profile', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function profile_updated()
    {
        $user_id = $this->session->userdata('user_id');
        $profile_data = $this->settings_model->array_from_post(array('fullname', 'phone', 'language', 'locale'));

        if (!empty($_FILES['avatar']['name'])) {
            $val = $this->settings_model->uploadImage('avatar');
            $val == TRUE || redirect('admin/front/settings/update_profile');
            $profile_data['avatar'] = $val['path'];
        }

        $this->settings_model->_table_name = 'tbl_account_details';
        $this->settings_model->_primary_key = 'user_id';
        $this->settings_model->save($profile_data, $user_id);

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $user_id,
            'activity' => ('activity_update_profile'),
            'value1' => $profile_data['fullname'],
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);

        $client_id = $this->input->post('client_id', TRUE);
        if (!empty($client_id)) {
            $client_data = $this->settings_model->array_from_post(array('name', 'email', 'address'));
            $this->settings_model->_table_name = 'tbl_client';
            $this->settings_model->_primary_key = 'client_id';
            $this->settings_model->save($client_data, $client_id);
        }
        $type = "success";
        $message = lang('profile_updated');
        set_message($type, $message);
        redirect('admin/front/settings/update_profile'); //redirect page
    }

    public function set_password()
    {
        $user_id = $this->session->userdata('user_id');
        $password = $this->hash($this->input->post('old_password', TRUE));
        $check_old_pass = $this->admin_model->check_by(array('password' => $password), 'tbl_users');
        $user_info = $this->admin_model->check_by(array('user_id' => $user_id), 'tbl_users');
        if (!empty($check_old_pass)) {
            $new_password = $this->input->post('new_password', true);
            $confirm_password = $this->input->post('confirm_password', true);
            if ($new_password == $confirm_password) {
                $data['password'] = $this->hash($new_password);
                $this->settings_model->_table_name = 'tbl_users';
                $this->settings_model->_primary_key = 'user_id';
                $this->settings_model->save($data, $user_id);
                $type = "success";
                $message = lang('password_updated');
                $action = ('activity_password_update');
            } else {
                $type = "error";
                $message = lang('password_does_not_match');
                $action = ('activity_password_error');
            }
        } else {
            $type = "error";
            $message = lang('password_error');
            $action = ('activity_password_error');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $user_id,
            'activity' => $action,
            'value1' => $user_info->username,
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        set_message($type, $message);
        redirect('admin/front/settings/update_profile'); //redirect page
    }

    public function change_email()
    {
        $user_id = $this->session->userdata('user_id');
        $password = $this->hash($this->input->post('password', TRUE));
        $check_old_pass = $this->settings_model->check_by(array('password' => $password), 'tbl_users');
        $user_info = $this->admin_model->check_by(array('user_id' => $user_id), 'tbl_users');
        if (!empty($check_old_pass)) {
            $new_email = $this->input->post('email', TRUE);
            if ($check_old_pass->email == $new_email) {
                $type = 'error';
                $message = lang('current_email');
                $action = lang('trying_update_email');
            } elseif ($this->is_email_available($new_email)) {
                $data = array(
                    'new_email' => $new_email,
                    'new_email_key' => md5(rand() . microtime()),
                );

                $this->settings_model->_table_name = 'tbl_users';
                $this->settings_model->_primary_key = 'user_id';
                $this->settings_model->save($data, $user_id);
                $data['user_id'] = $user_id;
                $this->send_email_change_email($new_email, $data);
                $type = "success";
                $message = lang('succesffuly_change_email');
                $action = lang('activity_updated_email');
            } else {
                $type = "error";
                $message = lang('duplicate_email');
                $action = ('trying_update_email');
            }
        } else {
            $type = "error";
            $message = lang('password_error');
            $action = ('trying_update_email');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $user_id,
            'activity' => $action,
            'value1' => $user_info->email,
            'value2' => $new_email,
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        set_message($type, $message);
        redirect('admin/front/settings/update_profile'); //redirect page
    }

    function send_email_change_email($email, $data)
    {
        $email_template = $this->settings_model->check_by(array('email_group' => 'change_email'), 'tbl_email_templates');
        $message = $email_template->template_body;
        $subject = $email_template->subject;

        $email_key = str_replace("{NEW_EMAIL_KEY_URL}", base_url() . 'login/reset_email/' . $data['user_id'] . '/' . $data['new_email_key'], $message);
        $new_email = str_replace("{NEW_EMAIL}", $data['new_email'], $email_key);
        $site_url = str_replace("{SITE_URL}", base_url(), $new_email);
        $message = str_replace("{SITE_NAME}", config_item('company_name'), $site_url);

        $params['recipient'] = $email;

        $params['subject'] = '[ ' . config_item('company_name') . ' ]' . ' ' . $subject;
        $params['message'] = $message;

        $params['resourceed_file'] = '';
        $this->settings_model->send_email($params);
    }

    function is_email_available($email)
    {

        $this->db->select('1', FALSE);
        $this->db->where('LOWER(email)=', strtolower($email));
        $this->db->or_where('LOWER(new_email)=', strtolower($email));
        $query = $this->db->get('tbl_users');
        return $query->num_rows() == 0;
    }

    public function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }

    public function change_username()
    {
        $user_id = $this->session->userdata('user_id');
        $password = $this->hash($this->input->post('password', TRUE));
        $check_old_pass = $this->admin_model->check_by(array('password' => $password), 'tbl_users');
        $user_info = $this->admin_model->check_by(array('user_id' => $user_id), 'tbl_users');
        if (!empty($check_old_pass)) {
            $data['username'] = $this->input->post('username');
            $this->settings_model->_table_name = 'tbl_users';
            $this->settings_model->_primary_key = 'user_id';
            $this->settings_model->save($data, $user_id);
            $type = "success";
            $message = lang('username_updated');
            $action = ('activity_username_updated');
        } else {
            $type = "error";
            $message = lang('password_error');
            $action = ('username_changed_error');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $user_id,
            'activity' => $action,
            'value1' => $user_info->username,
            'value2' => $this->input->post('username'),
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        set_message($type, $message);
        redirect('admin/front/settings/update_profile'); //redirect page
    }


    // general settings
    public function sync_frontend()
    {
        $all_frontend_page = get_result('front_cms_pages');
        if (!empty($all_frontend_page)) {
            foreach ($all_frontend_page as $v_page) {
                $descriptions = $v_page->description;
                $new_descriptions['description'] = str_replace("http://localhost/Ultimate_SaaS/", base_url(), $descriptions);

                // get all client
                $this->cms_frontend_model->_table_name = 'front_cms_pages';
                $this->cms_frontend_model->_primary_key = 'pages_id';
                $this->cms_frontend_model->save($new_descriptions, $v_page->pages_id);
            }
        }
        $col_1 = str_replace("http://localhost/non_saas/", base_url(), config_item('front_footer_col_1_description'));
        $col_2 = str_replace("http://localhost/non_saas/", base_url(), config_item('front_footer_col_2_description'));
        $col_3 = str_replace("http://localhost/non_saas/", base_url(), config_item('front_footer_col_3_description'));
        $col_4 = str_replace("http://localhost/non_saas/", base_url(), config_item('front_footer_col_4_description'));

        $input_data['front_footer_col_1_description'] = $col_1;
        $input_data['front_footer_col_2_description'] = $col_2;
        $input_data['front_footer_col_3_description'] = $col_3;
        $input_data['front_footer_col_4_description'] = $col_4;
        $input_data['sync_frontend'] = 'Done';
        $companies_id = $this->session->userdata('companies_id');
        if (empty($companies_id)) {
            $companies_id = null;
        }
        foreach ($input_data as $key => $value) {
            $data = array('value' => $value);
            $where = array('companies_id' => $companies_id, 'config_key' => $key);
            $this->db->where($where)->update('tbl_config', $data);
            $exists = $this->db->where($where)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("companies_id" => $companies_id, "config_key" => $key, "value" => $value));
            }
        }
        // messages for user
        $type = "success";
        $message = lang('save_general_settings');
        set_message($type, $message);
        redirect('admin/front/settings');
    }


    public function general_settings()
    {

        $input_data = $this->cms_frontend_model->array_from_post(array('front_site_name', 'front_slider', 'home_slider_speed', 'plan_expiry_date_alert'));

        //logo Process
        if (!empty($_FILES['front_favicon']['name'])) {
            $val = $this->cms_frontend_model->uploadImage('front_favicon');
            $val == TRUE || redirect('admin/front/settings');
            $input_data['front_favicon'] = $val['path'];
        }
        //favicon Process
        if (!empty($_FILES['front_nav_logo']['name'])) {
            $val = $this->cms_frontend_model->uploadImage('front_nav_logo');
            $val == TRUE || redirect('admin/front/settings');
            $input_data['front_nav_logo'] = $val['path'];
        }

        //header image
        if (!empty($_FILES['front_header_image']['name'])) {
            $val = $this->cms_frontend_model->uploadImage('front_header_image');
            $val == TRUE || redirect('admin/front/settings');
            $input_data['front_header_image'] = $val['path'];
        }

        $companies_id = $this->session->userdata('companies_id');
        if (empty($companies_id)) {
            $companies_id = null;
        }
        foreach ($input_data as $key => $value) {
            $data = array('value' => $value);
            $where = array('companies_id' => $companies_id, 'config_key' => $key);
            $this->db->where($where)->update('tbl_config', $data);
            $exists = $this->db->where($where)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("companies_id" => $companies_id, "config_key" => $key, "value" => $value));
            }
        }

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => ('activity_save_general_settings'),
            'value1' => $input_data['front_site_name']
        );
        $this->cms_frontend_model->_table_name = 'tbl_activities';
        $this->cms_frontend_model->_primary_key = 'activities_id';
        $this->cms_frontend_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_general_settings');
        set_message($type, $message);
        redirect('admin/front/settings');
    }


    public function payments($payment = NULL)
    {
        $data['page'] = lang('settings');
        $data['load_setting'] = 'payments';
        $data['title'] = "Payments"; //Page title
        $data['payment'] = $payment;
        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_payments($payment)
    {
        if ($payment == 'paypal') {
            $input_data = $this->settings_model->array_from_post(array('paypal_api_username', 'api_signature', 'paypal_live', 'paypal_status'));
            $paypal_api_password = $this->input->post('paypal_api_password', true);
            if (!empty($paypal_api_password)) {
                $input_data['paypal_api_password'] = encrypt($paypal_api_password);
            }

        } elseif ($payment == '2checkout') {
            $input_data = $this->settings_model->array_from_post(array('2checkout_publishable_key', '2checkout_private_key', '2checkout_seller_id', '2checkout_status'));
            $input_data ['two_checkout_live '] = $this->input->post('two_checkout_live', true);
            if (empty($input_data ['two_checkout_live '])) {
                $input_data ['two_checkout_live '] = 'FALSE';
            }
        } elseif ($payment == 'Stripe') {
            $input_data = $this->settings_model->array_from_post(array('stripe_private_key', 'stripe_public_key', 'bitcoin_address', 'stripe_status'));
        } elseif ($payment == 'bitcoin') {
            $input_data = $this->settings_model->array_from_post(array('bitcoin_address', 'bitcoin_status'));
        } elseif ($payment == 'Authorize.net') {
            $input_data = $this->settings_model->array_from_post(array('aim_api_login_id', 'aim_authorize_transaction_key', 'authorize_status'));
            $input_data ['aim_authorize_live '] = $this->input->post('aim_authorize_live', true);
            if (empty($input_data ['aim_authorize_live '])) {
                $input_data ['aim_authorize_live '] = 'FALSE';
            }
        } elseif ($payment == 'CCAvenue') {
            $input_data = $this->settings_model->array_from_post(array('ccavenue_merchant_id', 'ccavenue_key', 'ccavenue_access_code', 'ccavenue_enable_test_mode', 'ccavenue_status'));
        } elseif ($payment == 'Mollie') {
            $input_data = $this->settings_model->array_from_post(array('mollie_api_key', 'mollie_partner_id', 'mollie_status'));
        } elseif ($payment == 'PayUmoney') {
            $input_data = $this->settings_model->array_from_post(array('payumoney_enable_test_mode', 'payumoney_key', 'payumoney_salt', 'payumoney_status'));
        } else {
            $input_data = $this->settings_model->array_from_post(array('braintree_merchant_id', 'braintree_private_key', 'braintree_public_key', 'braintree_default_account', 'braintree_live_or_sandbox', 'braintree_status'));
        }

        $companies_id = $this->session->userdata('companies_id');
        if (empty($companies_id)) {
            $companies_id = null;
        }
        foreach ($input_data as $key => $value) {
            if (strtolower($value) == 'on') {
                $value = 'TRUE';
            } elseif (strtolower($value) == 'off') {
                $value = 'FALSE';
            }
            $data = array('value' => $value);
            $where = array('companies_id' => $companies_id, 'config_key' => $key);
            $this->db->where($where)->update('tbl_config', $data);
            $exists = $this->db->where($where)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("companies_id" => $companies_id, "config_key" => $key, "value" => $value));
            }
        }

        // messages for user
        $type = "success";
        $message = lang('payment_update_success');
        set_message($type, $message);
        redirect('admin/front/settings/payments');
    }

    // footer
    public function footer()
    {
        $data['title'] = lang('footer');
        $data['load_setting'] = 'footer';

        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    // save footer
    public function save_footer()
    {
        $input_data = $this->cms_frontend_model->array_from_post(array('front_footer_col_1_title', 'front_footer_col_1_description', 'front_footer_col_2_title', 'front_footer_col_2_description', 'front_footer_col_3_title', 'front_footer_col_3_description', 'front_footer_col_4_title', 'front_footer_col_4_description'));

        //header image
        if (!empty($_FILES['front_footer_bg']['name'])) {
            $val = $this->cms_frontend_model->uploadImage('front_footer_bg');
            $val == TRUE || redirect('admin/front/settings');
            $input_data['front_footer_bg'] = $val['path'];
        }

        foreach ($input_data as $key => $value) {
            $data = array('value' => $value);
            $this->db->where('config_key', $key)->update('tbl_config', $data);
            $exists = $this->db->where('config_key', $key)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
            }
        }

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => ('save_footer_settings'),
            'value1' => 'Footer'
        );
        $this->cms_frontend_model->_table_name = 'tbl_activities';
        $this->cms_frontend_model->_primary_key = 'activities_id';
        $this->cms_frontend_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_general_settings');
        set_message($type, $message);
        redirect('admin/front/settings/footer');
    }

    // footer
    public function subscribe()
    {
        $data['title'] = lang('subscribe');
        $data['load_setting'] = 'subscribe';

        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    // pricint
    public function pricing()
    {
        $data['title'] = lang('pricing');
        $data['load_setting'] = 'pricing';

        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    // save footer
    public function save_pricing()
    {
        $input_data = $this->cms_frontend_model->array_from_post(array('front_pricing_title', 'front_pricing_description'));


        foreach ($input_data as $key => $value) {
            $data = array('value' => $value);
            $this->db->where('config_key', $key)->update('tbl_config', $data);
            $exists = $this->db->where('config_key', $key)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
            }
        }

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => ('save_pricing_settings'),
            'value1' => 'pricing'
        );
        $this->cms_frontend_model->_table_name = 'tbl_activities';
        $this->cms_frontend_model->_primary_key = 'activities_id';
        $this->cms_frontend_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_pricing_settings');
        set_message($type, $message);
        redirect('admin/front/settings/pricing');
    }

    // save subscribe
    public function save_subscribe()
    {
        $input_data = $this->cms_frontend_model->array_from_post(array('front_subscribe_title', 'front_subscribe_description', 'front_subscribe'));

        foreach ($input_data as $key => $value) {
            $data = array('value' => $value);
            $this->db->where('config_key', $key)->update('tbl_config', $data);
            $exists = $this->db->where('config_key', $key)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
            }
        }

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => ('save_subscribe_settings'),
            'value1' => 'subscribe'
        );
        $this->cms_frontend_model->_table_name = 'tbl_activities';
        $this->cms_frontend_model->_primary_key = 'activities_id';
        $this->cms_frontend_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_general_settings');
        set_message($type, $message);
        redirect('admin/front/settings/subscribe');
    }

    // contact info
    public function contact()
    {
        $data['title'] = lang('contact');
        $data['load_setting'] = 'contact';

        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    // save contact
    public function save_contact()
    {
        $input_data = $this->cms_frontend_model->array_from_post(array('front_contact_title', 'front_contact_description', 'front_contact_location', 'front_contact_number', 'front_contact_email'));


        foreach ($input_data as $key => $value) {
            $data = array('value' => $value);
            $this->db->where('config_key', $key)->update('tbl_config', $data);
            $exists = $this->db->where('config_key', $key)->get('tbl_config');

            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
            }
        }

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => ('save_contact_settings'),
            'value1' => 'Contact'
        );
        $this->cms_frontend_model->_table_name = 'tbl_activities';
        $this->cms_frontend_model->_primary_key = 'activities_id';
        $this->cms_frontend_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_general_settings');
        set_message($type, $message);
        redirect('admin/front/settings/contact');
    }

    // social link
    public function social()
    {
        $data['title'] = lang('social_link');
        $data['load_setting'] = 'social';

        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    // save social link
    public function save_social()
    {

        $input_data = $this->cms_frontend_model->array_from_post(array('front_facebook_link', 'front_twitter_link', 'front_google_link', 'front_linkedin_link', 'front_pinterest_link', 'front_instagram_link'));

        foreach ($input_data as $key => $value) {
            $data = array('value' => $value);
            $this->db->where('config_key', $key)->update('tbl_config', $data);
            $exists = $this->db->where('config_key', $key)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
            }
        }

        // save activity
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => ('save_social_link'),
            'value1' => 'Social Link'
        );
        $this->cms_frontend_model->_table_name = 'tbl_activities';
        $this->cms_frontend_model->_primary_key = 'activities_id';
        $this->cms_frontend_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_social_link');
        set_message($type, $message);
        redirect('admin/front/settings/social');
    }

    // slider
    public function slider($id = null)
    {
        $data['title'] = lang('slider');
        $data['load_setting'] = 'slider';

        if (!empty($id)) {
            $data['active'] = 2;
            $data['slider_info'] = get_row('tbl_frontend_slider', array('id' => $id));
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    /*slider list*/
    public function slider_list()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_frontend_slider';
            $this->datatables->column_search = array('title');
            $this->datatables->order = array('id' => 'asc');
            $fetch_data = make_datatables();


            $data = array();
            $edited = can_action('154', 'edited');
            $deleted = can_action('154', 'deleted');

            foreach ($fetch_data as $_key => $info) {
                $action = null;
                $sub_array = array();

                $sub_array[] = $info->title;
                $sub_array[] = '<img style="width:200px;" src="' . base_url() . $info->slider_img . '">';

                $sub_array[] = $info->description;

                if ($info->status == 1) {
                    $sub_array[] = '<span class="label label-green">Activated</span>';
                } else {
                    $sub_array[] = '<span class="label label-danger">Deactivated</span>';
                }

                if (!empty($edited)) {
                    $action .= btn_edit('admin/front/settings/slider/' . $info->id) . ' ';
                }
                if (!empty($deleted)) {
                    $action .= ajax_anchor(base_url("admin/front/settings/delete_slider/$info->id"), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key));
                }

                $sub_array[] = $action;
                $data[] = $sub_array;
            }
            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    /*save slider*/
    public function save_slider($id = null)
    {
        $data = $this->cms_frontend_model->array_from_post(array('title', 'subtitle', 'description', 'button_icon_1', 'button_icon_1', 'button_text_1', 'button_link_1', 'button_text_2', 'button_link_2', 'status'));


        if (!empty($_FILES['slider_bg']['name'])) {
            $val = $this->cms_frontend_model->uploadImage('slider_bg');
            $val == TRUE || redirect('admin/front/settings/slider');
            $data['slider_bg'] = $val['path'];
        }

        if (!empty($_FILES['slider_img']['name'])) {
            $val = $this->cms_frontend_model->uploadImage('slider_img');
            $val == TRUE || redirect('admin/front/settings/slider');
            $data['slider_img'] = $val['path'];
        }
        $this->cms_frontend_model->_table_name = "tbl_frontend_slider"; // table name
        $this->cms_frontend_model->_primary_key = "id"; // $id
        $this->cms_frontend_model->save($data, $id);

        if (!empty($id)) {
            $activity = 'update_slider';
            $msg = lang($activity);
        } else {
            $activity = 'save_slider';
            $msg = lang($activity);
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => $activity,
            'value1' => $data['title']
        );

        $this->cms_frontend_model->_table_name = 'tbl_activities';
        $this->cms_frontend_model->_primary_key = 'activities_id';
        $this->cms_frontend_model->save($activity);

        // messages for user
        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/front/settings/slider');
    }

    /*delete slider*/
    public function delete_slider($id = null)
    {
        if ($id) {
            $slider_info = $this->cms_frontend_model->check_by(array('id' => $id), 'tbl_frontend_slider');

            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $this->session->userdata('user_id'),
                'activity' => ('activity_deleted_page'),
                'icon' => 'fa-user',
                'value1' => $slider_info->title
            );
            $this->cms_frontend_model->_table_name = 'tbl_activities';
            $this->cms_frontend_model->_primary_key = "activities_id";
            $this->cms_frontend_model->save($activities);

            // deletre into tbl_frontend_slider details by user id
            $this->cms_frontend_model->_table_name = 'tbl_frontend_slider';
            $this->cms_frontend_model->_primary_key = 'id';
            $this->cms_frontend_model->delete($id);

            if (is_file($slider_info->slider)) {
                unlink($slider_info->slider);
            }

            // messages for user
            $type = "success";
            $message = lang('delete') . " " . lang('slider');
        } else {
            $type = "error";
            $message = lang('no_permission');
        }

        echo json_encode(array("status" => $type, 'message' => $message));
        exit();
    }

    public function activities()
    {
        $data['title'] = lang('activities');
        $data['activities_info'] = $this->db->where(array('user' => $this->session->userdata('user_id')))->order_by('activity_date', 'DESC')->get('tbl_activities')->result();

        $data['subview'] = $this->load->view('admin/front/settings/activities', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function activitiesList($type = null)
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_activities';
            $this->datatables->column_order = array('activity_date', 'module', 'activity', 'value1');
            $this->datatables->column_search = array('activity_date', 'module', 'activity', 'value1');
            $this->datatables->order = array('activities_id' => 'desc');
            if (!empty($type)) {
                $where = array('user' => $this->session->userdata('user_id'), 'module' => $type);
            } else {
                $where = array('user' => $this->session->userdata('user_id'));
            }
            // get all invoice
            $fetch_data = get_result('tbl_activities', $where);

            $data = array();

            foreach ($fetch_data as $_key => $v_activity) {
                $action = null;
                $sub_array = array();
                $sub_array[] = display_datetime($v_activity->activity_date);
                $sub_array[] = fullname($v_activity->user);
                $sub_array[] = lang($v_activity->module);
                $sub_array[] = lang($v_activity->activity) . ' <strong>' . $v_activity->value1 . ' ' . $v_activity->value2 . '</strong>';
                $data[] = $sub_array;
            }

            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function clear_activities()
    {
        $this->db->where(array('user' => $this->session->userdata('user_id')))->delete('tbl_activities');
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => 'activity_deleted',
            'value1' => lang('all_activity') . ' ' . date('Y-m-d')
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);

        $type = "success";
        $message = lang('activities_deleted');
        set_message($type, $message);
        redirect('admin/dashboard');
    }

    public function system_update()
    {
        $data['page'] = lang('settings');
        $data['title'] = lang('system_update');
        if (!extension_loaded('curl')) {
            $data['update_errors'][] = 'CURL Extension not enabled';
            $data['latest_version'] = 0;
            $data['update_info'] = json_decode("");
        } else {
            $data['update_info'] = $this->admin_model->get_update_info();
            if (strpos($data['update_info'], 'Curl Error -') !== FALSE) {
                $data['update_errors'][] = $data['update_info'];
                $data['latest_version'] = 0;
                $data['update_info'] = json_decode("");
            } else {
                $data['update_info'] = json_decode($data['update_info']);
                $data['latest_version'] = $data['update_info']->latest_version;
                $data['update_errors'] = array();
            }
        }
        if (!extension_loaded('zip')) {
            $data['update_errors'][] = 'ZIP Extension not enabled';
        }
        $data['current_version'] = $this->db->get('tbl_migrations')->row()->version;
        $data['load_setting'] = 'system_update';
        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);

    }

    public function see_password($type = null)
    {
        $data['title'] = lang('see_password');
        $data['ids'] = null;
        $data['password'] = null;
        $data['row'] = null;
        if (!empty($type) && !is_numeric($type)) {
            $ex = explode('_', $type);
            if ($ex[0] == 'c') {
                $data['password'] = get_row('tbl_client', array('client_id' => $ex[1]), 'password');
            } elseif ($ex[0] == 'smtp') {
                $data['password'] = config_item('smtp_pass');
            } elseif ($ex[0] == 'emin') {
                $data['password'] = config_item('config_password');
            } elseif ($ex[0] == 'timap') {
                $data['password'] = get_row('tbl_departments', array('departments_id' => $ex[1]), 'password');
                $data['ids'] = $ex[1];
            } elseif ($ex[0] == 'paypalpassword') {
                $data['password'] = config_item('paypal_api_password');
            } elseif ($ex[0] == 'activationtoken') {
                $token = get_row('tbl_subscriptions', array('subscriptions_id' => $ex[1]), 'activation_tocken');
                $sub_domain = base_url() . '/setup?result=' . url_encode($ex[1]);
                $data['password'] = '<span>' . $token . '<br/>' . lang('url') . ': <a target="_blank" href="' . $sub_domain . '">' . $sub_domain . '</a></span>';
                $data['row'] = true;
            }
        }
        $data['subview'] = $this->load->view('admin/settings/see_password', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function new_currency($action = null, $code = null)
    {
        if (!empty($action)) {
            $data = $this->settings_model->array_from_post(array('code', 'name', 'symbol'));
            if (!empty($code)) {
                $this->db->set($data);
                $this->db->where('code', $code);
                $this->db->update('tbl_currencies');
                redirect('admin/front/settings/all_currency');
            } else {
                $this->settings_model->_table_name = 'tbl_currencies';
                $this->settings_model->save($data);
                redirect('admin/front/settings/system');
            }

        }
        $data['title'] = lang('activities');
        $data['modal_subview'] = $this->load->view('admin/front/settings/_modal_new_currency', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function all_currency($code = null)
    {
        $data['page'] = lang('settings');
        $data['load_setting'] = 'all_currency';
        $data['title'] = lang('all_currency'); //Page title
        if (!empty($code)) {
            $data['currency'] = $this->db->where('code', $code)->get('tbl_currencies')->row();
        }
        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function translations($lang = null)
    {
        $data['page'] = lang('settings');


        if (!empty($lang)) {
            $data['language'] = $lang;
            $data['language_files'] = $this->language_files;
        } else {
            $data['active_language'] = $this->settings_model->get_active_languages();
            $data['availabe_language'] = $this->settings_model->available_translations();
        }

        $data['translation_stats'] = $this->settings_model->translation_stats($this->language_files);

        $data['load_setting'] = 'translations';
        $data['title'] = lang('translations'); //Page title
        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function translations_status($language, $status)
    {
        $data['active'] = $status;
        $this->db->where('name', $language)->update('tbl_languages', $data);
        $type = 'success';
        if ($status == 1) {
            $message = lang('language_active_successfully');
        } else {
            $message = lang('language_deactive_successfully');
        }
        set_message($type, $message);
        redirect('admin/front/settings/translations');
    }

    public function add_language()
    {
        $language = $this->input->post('language', TRUE);
        $this->settings_model->add_language($language, $this->language_files);
        $type = 'success';
        $message = lang('language_added_successfully');
        set_message($type, $message);
        redirect('admin/front/settings/translations');
    }

    public function edit_translations($lang, $file)
    {

        $path = $this->language_files[$file . '_lang.php'];

        $data['language'] = $lang;
        //CI will record your lang file is loaded, unset it and then you will able to load another
        //unset the lang file to allow the loading of another file
        if (isset($this->lang->is_loaded)) {
            $loaded = sizeof($this->lang->is_loaded);
            if ($loaded < 3) {
                for ($i = 3; $i <= $loaded; $i++) {
                    unset($this->lang->is_loaded[$i]);
                }
            } else {
                for ($i = 0; $i <= $loaded; $i++) {
                    unset($this->lang->is_loaded[$i]);
                }
            }
        }
        $data['english'] = $this->lang->load($file, 'english', TRUE, TRUE, $path);
        if ($lang == 'english') {
            $data['translation'] = $data['english'];
        } else {
            $data['translation'] = $this->lang->load($file, $lang, TRUE, TRUE);
        }
        $data['active_language_files'] = $file;

        $data['load_setting'] = 'translations';
        $data['current_languages'] = $lang;

        $data['title'] = "Edit Translations"; //Page title
        $data['subview'] = $this->load->view('admin/front/settings/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function set_translations()
    {
        $jpost = array();
        $jsondata = json_decode(html_entity_decode($_POST['json']));
        foreach ($jsondata as $jdata) {
            $jpost[$jdata->name] = $jdata->value;
        }
        $jpost['_path'] = $this->language_files[$jpost['_file'] . '_lang.php'];
        $this->settings_model->save_translation($jpost);
        // messages for user
        $type = "success";
        $message = '<strong style=color:#000>' . $jpost['_language'] . '</strong>' . " Information Successfully Update!";
        set_message($type, $message);
        redirect('admin/front/settings/translations');
    }


}

?>