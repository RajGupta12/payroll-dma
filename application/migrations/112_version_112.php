<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Version_112 extends CI_Migration
{
    function __construct()
    {
        parent::__construct();
    }

    public function up()
    {
        $this->db->query("ALTER TABLE `tbl_frontend_slider` CHANGE `button_icon_1` `button_icon_1` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `button_icon_2` `button_icon_2` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `sort` `sort` INT(11) NULL DEFAULT NULL;");
        $this->db->query("UPDATE `tbl_config` SET `value` = '1.1.2' WHERE `tbl_config`.`config_key` = 'version';");
    }
}
