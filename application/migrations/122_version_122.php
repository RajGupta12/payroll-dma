<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Version_122 extends CI_Migration
{
    function __construct()
    {
        parent::__construct();
    }

    public function up()
    {
        $this->db->query("ALTER TABLE `tbl_languages` DROP `companies_id`;");
        $this->db->query("UPDATE `tbl_config` SET `value` = '1.2.2' WHERE `tbl_config`.`config_key` = 'version';");
    }
}
